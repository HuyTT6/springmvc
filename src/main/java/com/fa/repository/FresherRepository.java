package com.fa.repository;

import com.fa.model.Fresher;

public interface FresherRepository {
	
	boolean authenticate(Fresher fresher);

}
